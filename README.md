# PROJET APPLICATION BUDGET

# Réalisation

Il va falloir créer une tranche complète de la base de donnée jusqu'à l'application. Nous aurons donc 2 applications, le serveur node.js et le front en React.
---
# Organisation

## 1/Faire un diagramme de classe représentant les entités qui persisteront:
----
<img src="./image/diagrammeDeClasse.png" width="auto" height="300" />

## 2/Créer une script SQL avec création de la table et insertion de quelques données de tests
----
- Avant de créer une table, il faut définir ses colonnes, le type de chacune des colonnes et décider si elles peuvent ou non contenir NULL  
- Chaque table créée doit définir une clé primaire.
- j'ai pris la descision de nommer en minuscule sans accent, séparez par un _ et au singulier = COHERENCE


```
DROP TABLE IF EXISTS Gestion_de_budget;
CREATE TABLE `Gestion_de_budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(300) NOT NULL,
  `titre` varchar(256) DEFAULT NULL,
  `date_depense` date NOT NULL,
  `montant_depense` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO budget(id,titre,date_depense,montant_depense,categorie) 
VALUES(1,'achat de vêtements','2021-05-18',35.5,'vetement'),(2,'pique-nique','2021-05-18',55,'alimentation');
```
---
## 3/Créer l'application node.js/express
### Créer le contrôleur et les routes pour la table en question
-----

J\'ai créer le dossier API-PROJET-BUDGET:\
Comme on l'a vu plus haut j'ai fait le choix de commencer avec une seule table.
 
J\'ai tout d'abord mis en place la structure vide de mes dossiers et fichiers.
J\'ai mis en place la configuration server et créer le fichier.env.

  - Entities
  - Controller: il y a des methodes que je n'utilise pas mais c'est pour des foncitonnalités a developper ultérieurement (elles fonctionnent)
  - Repository: la connection est se fait sur le port || 8000
---
### Créer une application React et les différents components
---
Les commandes pour lancer un nouveau projet React:
npx create-react-app mon-app
cd mon-app
npm start

Construction de mon espace de travail avec les fichiers:
- components réutilisables
- pages: Home qui fait un total des depenses, avec un peu de CSS pour tester les imports de style.\
La page Débit qui affiche la BDD dans des cards avec une modal pour voir les détails.\
La page Formulaire qui fait appel à un component Form avec la librairie React-Bootstrap.\
La page Footer pour rajouter eventuellement son contenu plus tard.

- shared: avec les requêtes ajax 
---
### J\' utilisé différents techniques 
-----
- les appels Ajax dans le fichier depenses-service.js:npm i axios dans le terminal
  ex:
  
```
  import axios from "axios";

export class BudgetService {

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL + '/api/budget/all');
    }
  
  ```

  - react-bootstrap:
npm install react-bootstrap bootstrap@4.6.0

Afin d'installer des components il est important de réaliser les bons imports, la doc donne tous les details. La prise en main a été plutot facile.
Cela m'a permis d'avoir un rendu directement responsive.
\

