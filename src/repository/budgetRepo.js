import { Budget } from '../entity/Budget';
import { connection } from './bddconnection';

export class BudgetRepository {
    /**
     * trouver toutes les lignes d'un tableau
     * @returns 
     */
    async findAll() {
        const [rows] = await connection.execute('SELECT * FROM budget');
        const budgets = [];
        for (const row of rows) {
            let budget = new Budget(row.titre, row.date_depense, row.montant_depense, row.categorie, row.id);
            budgets.push(budget);
        }
        return budgets;
    }

    /**
     * @param {*} id 
     * @returns 
     */
    async findById(id) {
        const [rows] = await connection.execute(`SELECT * FROM budget WHERE Id=?`, [id]);
        let budgetId = [];
        for (const row of rows) {
            let instance = new Budget(row.titre, row.date_depense, row.montant_depense, row.categorie, row.id);
            budgetId.push(instance);
        }
        return budgetId
    }
    /**
    * add a row in the topic table via an object
    * @param {Object} budget
    */
    async add(budget) {
        await connection.query('INSERT INTO budget (titre, date_depense, montant_depense, categorie) VALUES (?, ?, ?, ?)', [budget.titre, budget.date_depense, budget.montant_depense, budget.categorie]);
    }

    async update(budget) {
       const [rows] = await connection.execute('UPDATE budget SET titre=?, date_depense=?, montant_depense=?, categorie=? WHERE id=?', [budget.titre, budget.date_depense, budget.montant_depense, budget.categorie, budget.id]);
    }

    async delete(id) {
        const [rows] = await connection.execute(`DELETE FROM budget WHERE id=?`, [id]);
    }

    async total(){
        const [rows] = await connection.query(`SELECT SUM(montant_depense) AS somme FROM budget`);
       
        return rows[0].somme 
    }

};