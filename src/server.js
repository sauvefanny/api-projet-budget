import express from "express";
import { budgetController } from './controller/budgetController';
import cors from 'cors';

export const server = express();

server.use(express.json());
server.use(cors());


server.get('/', async (req, res) => {
    res.json('Bienvenue')
});

server.use('/api/budget', budgetController);
