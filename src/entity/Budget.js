export class Budget {
    id;
    titre;
    date_depense;
    montant_depense;
    categorie;
    /**
     * @param {number} id         
     * @param {String} titre
     * @param {date} date_depense
     * @param {number} montant_depense
     * @param {String} categorie
     */
    constructor(titre, date_depense, montant_depense, categorie, id = null) {
        this.id = id;
        this.titre = titre;
        this.date_depense = date_depense;
        this.montant_depense = montant_depense;
        this.categorie = categorie;

    }
};
